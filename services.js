const {
    sendRequest
} = require('./http-client');

const getGames = async () => {
    console.info(`Sending request to IO for all games ...`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/games`
    }

    const games = await sendRequest(options);

    return games;
};

const getGamesById = async (id) => {
    console.info(`Sending request to IO for game ${id} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/games/id=${id}`
    }

    const game = await sendRequest(options);

    return game;
};

const getGamesByGenre = async (genre) => {
    console.info(`Sending request to IO for genre ${genre} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/games/genre=${genre}`
    }

    const game = await sendRequest(options);

    return game;
};

const addGame = async (name, genre, description) => {
    console.info(`Sending request to IO to add game with name ${name}, genre ${genre}, description ${description} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/games`,
        method: 'POST',
        data: {
            name,
            genre, 
            description
        }
    }

    const id = await sendRequest(options);

    return id;
};  

module.exports = {
    getGames,
    getGamesById,
    addGame,
    getGamesByGenre
}
