const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    getGamesById,
    getGames,
    addGame,
    getGamesByGenre
} = require('./services.js');


Router.get('/', async (req, res) => {
    
    const games = await getGames();

    res.json(games);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;

    const game = await getGamesById(id);

    res.json(game);
});

Router.get('/:genre', async (req, res) => {

    const {
        genre
    } = req.params;

    const game = await getGamesByGenre(genre);

    res.json(game);
});

Router.post('/', async (req, res) => {

    const {
        name,
        genre,
        description
    } = req.body;

    if (!name) {
        throw new ServerError('No name provided!', 400);
    }

    if (!description) {
        throw new ServerError('No description provided!', 400);
    }

    if (!genre) {
        throw new ServerError('No genre provided!', 400);
    }

    const id = await addGame(title, genre, description);

    res.json(id);
});


module.exports = Router;
